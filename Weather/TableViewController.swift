//
//  TableViewController.swift
//  
//
//  Created by Javier Ramon Gil on 19/6/15.
//
//

import UIKit
import CoreLocation

class TableViewController: UITableViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var location:CLLocation!
    var jsonArray = []    
    let cellTableIdentifier = "Reuse Cell"

    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestWhenInUseAuthorization()
        locationManager.pausesLocationUpdatesAutomatically = true
        
        navigationController?.navigationBar.barTintColor = UIColor.redColor()
        self.title = "Weather"
        
        let nib = UINib(nibName: "TableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellTableIdentifier)
        
        let jsonUrl: NSURL = NSURL(string: "http://pixel.uji.es/php/getjsonStations.php")!
        let jsonResultsData = NSData(contentsOfURL: jsonUrl, options: NSDataReadingOptions.DataReadingUncached, error: nil)
        let jsonResultsString:NSString = NSString(data: jsonResultsData!, encoding: NSUTF8StringEncoding)!
        var jsonResultsDict:NSDictionary = NSJSONSerialization.JSONObjectWithData(jsonResultsData!, options: nil, error: nil) as! NSDictionary
        jsonArray = jsonResultsDict["data"] as! NSArray
        
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status{
        case .AuthorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        dispatch_async(
            dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                
                self.location = (locations as! [CLLocation])[locations.count - 1]
                
                self.jsonArray = sorted(self.jsonArray, {
                    item1, item2 in
                    let location1 = self.getLocationByStringCoordinates(item1["LATITUD"] as? String, item1["LONGITUD"] as? String)
                    let location2 = self.getLocationByStringCoordinates(item2["LATITUD"] as? String, item2["LONGITUD"] as? String)
                    return self.locationManager.location.distanceFromLocation(location1) <
                        self.locationManager.location.distanceFromLocation(location2)
                })
                
                
        })
        
    
    
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        let errorType = error.code == CLError.Denied.rawValue ? "Acces Denied": "Error \(error.code)"
        let alertController = UIAlertController(title: "Location Manager Error", message: errorType, preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: {action in})
    }
    

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellTableIdentifier, forIndexPath: indexPath) as! TableViewCell

        // Configure the cell...
        cell.name = jsonArray [indexPath.row]["NAME"] as! String
        cell.province = jsonArray[indexPath.row]["PROVINCIA"] as! String
        
        var json = jsonArray[indexPath.row]["T"] as? String
        if (json != nil){
            cell.T = json!
        } else {
            cell.T = ""
        }
        
        var Pstr = jsonArray[indexPath.row]["P"] as? String
        if (Pstr != nil){
            var Pformatter = NSNumberFormatter().numberFromString(Pstr!)
            var P = Pformatter!.intValue
            
            if (P == 0){
                cell.myImage.image = UIImage(named: "sun")
            } else if (P > 0 && P < 1){
                cell.myImage.image = UIImage(named: "rainy")
            } else {
                cell.myImage.image = UIImage(named: "rain")
            }
        }else {
            cell.myImage.image = nil
        }
        
        return cell
    }
    
    func getLocationByStringCoordinates(latitude: String?, _ longitude: String?) -> CLLocation {
        if let latitude = latitude, let longitude = longitude {
            if let latitudeValue = NSNumberFormatter().numberFromString(latitude)?.doubleValue, let longitudeValue = NSNumberFormatter().numberFromString(longitude)?.doubleValue {
                return CLLocation(latitude: latitudeValue, longitude: longitudeValue)
            }
        }
        return CLLocation(latitude: 0, longitude: 0)
    }

}
