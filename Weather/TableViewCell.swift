//
//  TableViewCell.swift
//  Weather
//
//  Created by Javier Ramon Gil on 19/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var provinceLabel:UILabel!
    @IBOutlet var TLabel:UILabel!
    @IBOutlet var myImage:UIImageView!
    
    var name: String = ""{
        didSet{
            if (name != oldValue){
                nameLabel.text = name
            }
        }
    }
    var province: String = ""{
        didSet{
            if (province != oldValue){
                provinceLabel.text = province
            }
        }
    }
    var T: String = ""{
        didSet{
            if (T != oldValue){
                TLabel.text = T
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
